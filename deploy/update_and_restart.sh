# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/ubuntu/node-js-starter/

# clone the repo again
git clone https://gitlab.com/Doctor-28/node-js-starter

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
source /home/ubuntu/.nvm/nvm.sh

# stop the previous pm2
pm2 kill


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
# starting pm2 daemon
pm2 status

cd /home/ubuntu/node-js-starter

#install npm packages
echo "Running npm install"
npm install


#Restart the node server
echo "Restarting server..."
pm2 start npm -- start