echo "Starting deploy..."

# any future command that fails will exit the script
set -e

# Lets write the public key of our aws instance
mkdir -p ~/.ssh
touch ~/.ssh/id_rsa

echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa

# disable the host key checking.
chmod +x ./deploy/disable_hostkey.sh
./deploy/disable_hostkey.sh

# we have already setup the DEPLOYER_SERVER in our gitlab settings which is a
# comma seperated values of ip addresses.
DEPLOY_SERVER=$DEPLOY_SERVER


echo "deploying to ${DEPLOY_SERVER}"
ssh ubuntu@${DEPLOY_SERVER} 'bash' < ./deploy/update_and_restart.sh
